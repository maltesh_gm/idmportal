﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MongoDB;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Bson.Serialization;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Utilities;
using System.Collections.ObjectModel;
using log4net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Threading.Tasks;
using Philips.HealthCare.IDM.Portal.Logger;


namespace Philips.HealthCare.IDM.Portal.DataAccess
{
    public class MongoDbHelper
    {
        private static ILog Logger = log4net.LogManager.GetLogger("IDM.Portal.DataAccess");
        private static string databaseName = ConfigurationManager.AppSettings["MongoDbDatabaseName"];
        private static string siteCollectionName = ConfigurationManager.AppSettings["MongoDbSiteCollectionName"];
        private static string nameSpaceCollectionName = ConfigurationManager.AppSettings["MongoDbNamespaceCollectionName"];
        private static string eventCollectionName = ConfigurationManager.AppSettings["MongoDbMessageCollectionName"];
        private static string swdCollectionName = ConfigurationManager.AppSettings["MongoDbSWDCollectionName"];
        private static string swdeploymentCollectionName = ConfigurationManager.AppSettings["MongoDbSWDeployCollectionName"];
        private static string oneEMSCollectionName = ConfigurationManager.AppSettings["MongoDbOneEMSCollectionName"];
        private static string statesCollectionName = ConfigurationManager.AppSettings["MongoDbStatesCollectionName"];
        private static string countrylistCollectionName = ConfigurationManager.AppSettings["MongoDbCountryListCollectionName"];
        private readonly string monitors = ConfigurationManager.AppSettings["HostMonitors"];
        private readonly string nebNodeNamespace = ConfigurationManager.AppSettings["ThrukIPNamespace"];
        private readonly string thrukViewUrl = ConfigurationManager.AppSettings["SiteThrukURL"];
        private readonly string eventsDuration = ConfigurationManager.AppSettings["EventsDuration"];

        MongoCollection<BsonDocument> eventCollection, siteCollection, nameSpaceCollection,
               oneEMSTicketsCollection, statesCollection, swdeploymentCollection, countrylistCollection,swdCollection;
        Dictionary<string, string> oneEMSAllCasesCollection = new Dictionary<string, string>();
        #region Ctor
        /// <summary>
        /// Constructor
        /// </summary>
        public MongoDbHelper()
        {
            this.eventCollection = GetSpecificCollection(databaseName, eventCollectionName);
            this.siteCollection = GetSpecificCollection(databaseName, siteCollectionName);
            this.nameSpaceCollection = GetSpecificCollection(databaseName, nameSpaceCollectionName);
            this.oneEMSTicketsCollection = GetSpecificCollection(databaseName, oneEMSCollectionName);
            this.oneEMSAllCasesCollection = GetAllCasesCollection(oneEMSTicketsCollection);
            this.statesCollection = GetSpecificCollection(databaseName, statesCollectionName);
            this.countrylistCollection = GetSpecificCollection(databaseName, countrylistCollectionName);
            this.swdeploymentCollection = GetSpecificCollection(databaseName, swdeploymentCollectionName);
            this.swdCollection = GetSpecificCollection(databaseName, swdCollectionName);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// NotificationMessageDeserializer
        /// </summary>
        /// <param name="cursor">Instance of the MongoCursor</param>
        /// <returns>list of Notificaiton Messages</returns>
        public static List<NotificationMessage> NotificationMessageDeserializer(MongoCursor<BsonDocument> cursor)
        {
            List<NotificationMessage> allMessages = new List<NotificationMessage>();
            foreach (var doc in cursor)
            {
                allMessages.Add(BsonSerializer.Deserialize<NotificationMessage>(doc));
            }
            return allMessages;
        }

        #region GetAllSites
        /// <summary>
        /// GetAllSites
        /// </summary>
        /// <returns>All the Sites with names and their product version</returns>
        public List<SiteInfo> GetAllSites()
        {
            var eventCollection = GetSpecificCollection(databaseName, eventCollectionName);
            var siteCollection = GetSpecificCollection(databaseName, siteCollectionName);
            List<SiteInfo> sites = new List<SiteInfo>();
            if (eventCollection != null)
            {
                sites.Add(new SiteInfo { siteid = "All", sitename = "Not Applicable", siteversion = "NA" });
                var data = from s in GetAllSites(eventCollection) orderby s select s;
                foreach (var item in data) //mongoClient.Distinct("siteid"))
                {
                    if (!item.IsBsonNull)
                    {
                        BsonDocument doc = GetSiteInformation(siteCollection, item.AsString);
                        if (doc != null)
                        {
                            sites.Add(new SiteInfo
                            {
                                siteid = item.AsString,
                                sitename = doc.GetValue("sitename").ToString(),
                                siteversion = doc.GetValue("siteversion").ToString().Replace(",", "."),
                                country = new Country
                                {
                                    country = doc.GetValue("country").ToString(),
                                    market = doc.GetValue("market").ToString(),
                                    sap_code = doc.GetValue("sap_code").ToString()
                                }
                            });
                        }
                        else
                        {
                            sites.Add(new SiteInfo
                            {
                                siteid = item.AsString,
                                sitename = "Not Available",
                                siteversion = "NA",
                                country = new Country
                                {
                                    country = string.Empty,
                                    market = string.Empty,
                                    sap_code = string.Empty
                                }
                            });
                        }
                    }
                }
            }
            return sites;
        }
        #endregion

        public List<SiteInfo> GetSitesByCoutry(List<string> countryList)
        {
            var eventCollection = GetSpecificCollection(databaseName, eventCollectionName);
            var siteCollection = GetSpecificCollection(databaseName, siteCollectionName);
            List<SiteInfo> sites = new List<SiteInfo>();
            if (eventCollection != null)
            {
                sites.Add(new SiteInfo { siteid = "All", sitename = "Not Applicable", siteversion = "NA" });
                var data = from s in GetAllSites(eventCollection) orderby s select s;
                foreach (var item in data) //mongoClient.Distinct("siteid"))
                {
                    if (!item.IsBsonNull)
                    {
                        BsonDocument doc = GetSiteInformation(siteCollection, item.AsString);
                        if (doc != null)
                        {
                            if (countryList.Contains(doc.GetValue("country").ToString()))
                            {
                                sites.Add(new SiteInfo
                                {
                                    siteid = item.AsString,
                                    sitename = doc.GetValue("sitename").ToString(),
                                    siteversion = doc.GetValue("siteversion").ToString().Replace(",", "."),
                                    country = new Country
                                    {
                                        country = doc.GetValue("country").ToString(),
                                        market = doc.GetValue("market").ToString(),
                                        sap_code = doc.GetValue("sap_code").ToString()
                                    }
                                });
                            }
                        }
                    }
                }
            }
            return sites;
        }

        #region GetServiceFilters
        /// <summary>
        /// GetAllSites
        /// </summary>
        /// <returns>All the Sites with names and their product version</returns>
        public string GetServiceFilters()
        {
            var namespaceCollection = GetSpecificCollection(databaseName, nameSpaceCollectionName);
            IList<ServiceFilter> serviceNamespaceFilters = new List<ServiceFilter>();
            if (namespaceCollection != null)
            {
                serviceNamespaceFilters.Add(new ServiceFilter { servicedisplayname = "All" });
                foreach (var item in GetServiceFilters(namespaceCollection)) //mongoClient.Distinct("siteid"))
                {
                    if (!item.IsBsonNull)
                    {
                        serviceNamespaceFilters.Add(new ServiceFilter { servicedisplayname = item.AsBsonValue.ToString() });
                    }
                }
                return serviceNamespaceFilters.ToJson();
            }
            return string.Empty;
        }
        #endregion

        #region GetSystemFilters
        public SytemFilter GetSystemFilters()
        {
            SytemFilter filter = new SytemFilter();
            if (nameSpaceCollection != null && eventCollection != null && siteCollection != null)
            {
                filter.servicefilters.Add(new ServiceFilter { servicedisplayname = "All" });
                foreach (var item in GetServiceFilters(nameSpaceCollection))
                {
                    filter.servicefilters.Add(new ServiceFilter { servicedisplayname = item.AsBsonValue.ToString() });
                }
                //
                filter.sites.Add(new SiteInfo { siteid = "All", sitename = "Not Applicable", siteversion = "NA" });
                foreach (var item in GetAllSites(eventCollection))
                {
                    BsonDocument siteInfo = GetSiteInformation(siteCollection, item.AsString);
                    filter.sites.Add(
                        new SiteInfo
                        {
                            siteid = item.AsString,
                            sitename = siteInfo.GetValue("sitename").ToString(),
                            siteversion = siteInfo.GetValue("siteversion").ToString()
                        }
                    );
                }
                //
                filter.status.Add(new Status { status = "All" });
                foreach (var item in GetAllStatus(eventCollection))
                {
                    filter.status.Add(new Status { status = item.AsBsonValue.ToString() });
                }
                return filter;
            }
            return null;
        }
        #endregion

        #region GetAllMessages
        /// <summary>
        /// GetAllMessages
        /// </summary>
        /// <returns>all the messages</returns>
        public string GetAllMessages()
        {
            var mongoClient = GetSpecificCollection(databaseName, eventCollectionName);
            if (mongoClient != null)
            {
                var messages = mongoClient.FindAll().
                    SetFields(Fields.Exclude("_id", "timestamp")).
                    SetSortOrder(SortBy.Descending(new string[] { "timestamp" }));
                return messages.ToJson();
            }
            return String.Empty;
        }
        #endregion

        #region GetAllServiceNamespaces
        public List<ServiceNamespace> GetAllServiceNamespaces()
        {
            var mongoClient = GetSpecificCollection(databaseName, nameSpaceCollectionName);
            if (mongoClient != null)
            {
                return nameSpaceCollection.FindAs<ServiceNamespace>
                    (Query.EQ("indexes", BsonNull.Value))
                    .SetFields(Fields.Exclude("_id")).ToList<ServiceNamespace>();
            }
            return null;
        }
        #endregion

        #region GetCountryList
        public List<Country> GetCountryList()
        {
            var mongoClient = GetSpecificCollection(databaseName, countrylistCollectionName);
            if (mongoClient != null)
            {
                return countrylistCollection.FindAllAs<Country>().SetFields(Fields.Exclude("_id")).ToList<Country>();
            }

            return null;
        }
        #endregion

        #region GetAllNodes
        /// <summary>
        /// GetAllNodes
        /// </summary>
        /// <returns>all the nodes</returns>
        public string GetAllNodes()
        {
            var mongoClient = GetSpecificCollection(databaseName, eventCollectionName);
            if (mongoClient != null)
            {
                List<HostInfo> hosts = new List<HostInfo>();
                foreach (var item in GetAllNodes(mongoClient, null))
                {
                    hosts.Add(new HostInfo { hostname = item.AsString });
                }
                return hosts.ToJson();
            }
            return string.Empty;
        }
        #endregion

        #region GetAllNodesForSite
        /// <summary>
        /// GetAllNodesForSite
        /// </summary>
        /// <param name="siteid">siteid</param>
        /// <returns>all the nodes fora given site</returns>
        public string GetAllNodesForSite(string siteid)
        {
            var mongoClient = GetSpecificCollection(databaseName, eventCollectionName);
            if (mongoClient != null)
            {
                List<HostInfo> hosts = new List<HostInfo>();
                foreach (var item in GetAllNodes(mongoClient, siteid))
                {
                    hosts.Add(new HostInfo { hostname = item.AsString });
                }
                return hosts.ToJson();
            }
            return string.Empty;
        }
        #endregion

        #region GetAllMessagesForSite
        /// <summary>
        /// GetAllMessagesForSite
        /// </summary>
        /// <param name="siteid">siteid</param>
        /// <returns>all the messages for a given site</returns>
        public string GetAllMessagesForSite(string siteid)
        {
            var mongoClient = GetSpecificCollection(databaseName, eventCollectionName);
            if (mongoClient != null)
            {
                return GetAllMessages(mongoClient, siteid).ToJson();
            }
            return string.Empty;
        }
        #endregion

        #region GetAllMessagesForNode
        /// <summary>
        /// GetAllMessagesForNode
        /// </summary>
        /// <param name="nodeName">nodeName</param>
        /// <returns>all the messages for a given node name</returns>
        public string GetAllMessagesForNode(string nodeName)
        {
            var mongoClient = GetSpecificCollection(databaseName, eventCollectionName);
            if (mongoClient != null)
            {
                return GetAllMessagesForNode(mongoClient, nodeName).ToJson();
            }
            return string.Empty;
        }
        #endregion

        #region GetDefaultServiceEvents
        /// <summary>
        /// GetDefaultServiceEvents
        /// </summary>
        /// <returns>returns the default services events in json format</returns>
        public List<Message> GetDefaultServiceEvents()
        {
            SearchCriteria criteria = new SearchCriteria();
            criteria.siteid = "All";
            criteria.status = "CRITICAL";
            criteria.service = "All";
            criteria.startDate = DateTime.MinValue;
            criteria.endDate = DateTime.MaxValue;
            return FilteredServiceEvents(criteria);
        }
        #endregion

        #region GetEventHistory
        public MongoCursor<NotificationMessage> GetEventHistory(SearchCriteria criteria)
        {
            UserFilter filter;
            try
            {
                filter = new UserFilter();
                filter.beginTime = criteria.startDate;
                filter.endTime = criteria.endDate;
                if (string.Compare(criteria.siteid, "All", true) != 0)
                {
                    filter.siteid = criteria.siteid;
                }
                if (string.Compare(criteria.status, "All", true) != 0)
                {
                    filter.status = criteria.status;
                }
                if (string.Compare(criteria.service, "All", true) != 0)
                {
                    filter.serviceList = GetAllServices(nameSpaceCollection, criteria.service);
                }
                return GetEventsByDateRange(eventCollection, filter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                filter = null;
            }
            return null;
        }
        #endregion

        #region InsertSWDConfiguration
        public string InsertSWDConfiguration(SiteMapObject swdRequest)
        {
            var client = GetConnection();
            if (client == null)
            {
                //TODO: Write to a file
            }
            try
            {
                //if (!client.GetServer().DatabaseExists(LogDatabasename)) {}
                var mongoDb = client.GetServer().GetDatabase(databaseName);
                if (!mongoDb.CollectionExists(swdCollectionName))
                {
                    //Making a capped collection of 100MB                    
                    mongoDb.CreateCollection(swdCollectionName);
                }
                var swdCollection = mongoDb.GetCollection(swdCollectionName);
                swdCollection.Insert(swdRequest);
            }
            catch (Exception ex)
            {
                //TODO: handle
            }
            return "OK";
        }
        #endregion

        /// <summary>
        /// RestartSummaryCount
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public List<SiteRestartSummary> RestartSummaryCount(SearchCriteria criteria)
        {

            object siteMatch = null;
            if (string.Compare(criteria.siteid, "All", true) != 0)
            {
                siteMatch = new BsonDocument {
                   {
                    "$match", 
                    new BsonDocument 
                    {                         
                        {
                            "siteid" , criteria.siteid
                        }                            
                    } 
                }
               };
            };

            var serviceMatch = new BsonDocument {            
                {
                    "$match", 
                    new BsonDocument 
                    {                         
                        {
                            "service" , "Product__IntelliSpace__PACS__iSyntaxServer__Aggregate"
                        }                            
                    }                 
               }
            };

            var sort = new BsonDocument 
           {
               {
                   "$sort", new BsonDocument 
                   {
                       {
                           "siteid", 1
                       },
                       {
                           "hostname", 1
                       },                    
                       {
                           "timestamp", 1
                       }
                   }
               }
           };

            var dateRangematch = new BsonDocument
            {
                {
                    "$match", 
                    new BsonDocument 
                    {                         
                        {
                            "timestamp" , new BsonDocument 
                            {
                                {
                                    "$gte", criteria.startDate
                                },
                                {
                                    "$lte", criteria.endDate
                                }
                            }
                        }                            
                    } 
                }
            };

            var project = new BsonDocument { 
            { 
                "$project", new BsonDocument { 
                    {
                        "siteid", 1
                    },
                    {
                        "hostname", 1
                    },
                    {
                        "service", 1
                    }, 
                    {
                        "status", 1
                    }, 
                    {
                        "timestamp", 1
                    }, 
                    {
                        "type", 1
                    },
                    {
                        "_id", 0
                    }
                    } 
            } 
            };

            var pipeline = new[] {
                siteMatch.ToBsonDocument(),
                dateRangematch.ToBsonDocument(),
                serviceMatch.ToBsonDocument(),
                sort, project 
            };

            var args = new AggregateArgs { AllowDiskUse = true, Pipeline = pipeline };
            var result = eventCollection.Aggregate(args);
            var output = result.Select(BsonSerializer.Deserialize<NotificationMessage>);

            List<SiteRestartSummary> siteRestartSummaryCollection = new List<SiteRestartSummary>();
            SiteRestartSummary siteRestartInfo = null;
            HostRestartSummary hostRestartInfo = null;
            string siteid = string.Empty;
            string hostname = string.Empty;

            bool isRestartIssued = false;

            using (IEnumerator<NotificationMessage> enumerator = output.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    NotificationMessage nm = enumerator.Current;
                    if (siteid != nm.siteid)
                    {
                        siteRestartInfo = new SiteRestartSummary();
                        siteRestartInfo.siteid = nm.siteid;
                        var document = GetSiteInformation(siteCollection, nm.siteid);
                        string siteName = document != null ?
                            document.GetElement("sitename").Value.ToString() : "Not Available";
                        siteRestartInfo.sitename = siteName;
                        siteRestartSummaryCollection.Add(siteRestartInfo);
                    }
                    siteid = nm.siteid;

                    //if (hostname != nm.hostname)
                    if (string.Compare(hostname, nm.hostname, true) != 0)
                    {
                        isRestartIssued = false;
                        hostRestartInfo = new HostRestartSummary();
                        siteRestartInfo.hostrestartsummary.Add(hostRestartInfo);
                    }

                    hostname = nm.hostname;
                    hostRestartInfo.hostname = nm.hostname;

                    if (nm.type == "HANDLER" && nm.status == "CRITICAL")
                    {
                        if (!isRestartIssued)
                        {
                            isRestartIssued = true;
                            //hostRestartInfo.totalattempts++;
                            //siteRestartInfo.totalattempts++;
                        }
                        //else
                        //{
                        //    hostRestartInfo.failureattempts++;
                        //    hostRestartInfo.totalattempts++;
                        //    siteRestartInfo.failureattempts++;
                        //    siteRestartInfo.totalattempts++;
                        //}
                    }
                    else
                    {
                        if (isRestartIssued)
                        {
                            if (nm.type == "HANDLER" && nm.status == "OK")
                            {
                                hostRestartInfo.successattempts++;
                                hostRestartInfo.totalattempts++;
                                siteRestartInfo.totalattempts++;
                                siteRestartInfo.successattempts++;
                                
                                isRestartIssued = false;
                            }
                            else
                            {
                                hostRestartInfo.totalattempts++;
                                hostRestartInfo.failureattempts++;
                                siteRestartInfo.totalattempts++;
                                siteRestartInfo.failureattempts++;
                                
                                if (nm.type == "PROBLEM" && nm.status == "CRITICAL")
                                {
                                    isRestartIssued = false;
                                }
                            }
                        }
                    }
                }
            }
            return siteRestartSummaryCollection;
        }


        #region deployment Filters
        /// <summary>
        /// SWDeploymentSearchSummary
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns>SWDeployment</returns>
        public MongoCursor<SWDeployment> SWDeploymentSearchSummary(SearchCriteria criteria)
        {
            UserFilter filter;

            try
            {
                filter = new UserFilter();
                filter.beginTime = criteria.startDate;
                filter.endTime = criteria.endDate;

                if (string.Compare(criteria.siteid, "All", true) != 0)
                {
                    filter.siteid = criteria.siteid;
                }
                if (string.Compare(criteria.packagename, "All", true) != 0)
                {
                    filter.packagename = criteria.packagename;
                }
                return GetSWDeploymentsByDateRange(swdeploymentCollection, filter);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// GetSWDeploymentsByDateRange
        /// </summary>
        /// <param name="mongoCollection"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        private MongoCursor<SWDeployment> GetSWDeploymentsByDateRange(MongoCollection<BsonDocument> mongoCollection, UserFilter filter)
        {
            return mongoCollection.FindAs<SWDeployment>(BuildSWDeploymentQuery(filter)).SetFields(Fields.Exclude("_id")).SetSortOrder(SortBy.Descending(new string[] { "createddate" }));
        }


        /// <summary>
        /// BuildSWDeploymentQuery
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        private static IMongoQuery BuildSWDeploymentQuery(UserFilter filter)
        {
            double utcOffset = DateTimeOffset.Now.Offset.TotalDays;
            var queries = new List<IMongoQuery>();
            if (!String.IsNullOrEmpty(filter.siteid))
            {
                queries.Add(Query.EQ("siteid", filter.siteid));
            }
            if (!String.IsNullOrEmpty(filter.packagename))
            {
                queries.Add(Query.EQ("packagename", filter.packagename));
            }
            if (filter.beginTime != DateTime.MinValue)
            {
                 queries.Add(Query.GTE("createddate", filter.beginTime));
               
            }
            if (filter.endTime != DateTime.MinValue)
            {
                queries.Add(Query.LTE("createddate", filter.endTime.AddDays(1)));
                
            }
                    
            if (queries.Count == 0)
            {
                return null;
            }
            var queryBuilder = new QueryBuilder<BsonDocument>();
            return queryBuilder.And(queries);
        }
#endregion


        #region Distribution Filter
        /// <summary>
        /// SWDistributionSearchSummary
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns>SWDRequestMsg</returns>
        public MongoCursor<SWDRequestMsg> SWDistributionSearchSummary(SearchCriteria criteria)
        {
            UserFilter filter;

            try
            {
                filter = new UserFilter();
                filter.beginTime = criteria.startDate;
                filter.endTime = criteria.endDate;

                if (string.Compare(criteria.siteid, "All", true) != 0)
                {
                    filter.siteid = criteria.siteid;
                    // filter.siteid =mongohelper.GetAllSites();
                }
                if (string.Compare(criteria.packagename, "All", true) != 0)
                {
                    filter.packagename = criteria.packagename;
                }
                if (string.Compare(criteria.country, "All", true) != 0)
                {
                    filter.country = criteria.country;
                }

                return GetSWDistributionByDateRange(swdCollection, filter);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// GetSWDeploymentsByDateRange
        /// </summary>
        /// <param name="mongoCollection"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        private MongoCursor<SWDRequestMsg> GetSWDistributionByDateRange(MongoCollection<BsonDocument> mongoCollection, UserFilter filter)
        {
            // myList
            //.Where(stringToCheck => stringToCheck.Contains(myString));

            return mongoCollection.FindAs<SWDRequestMsg>(BuildSWDistributionQuery(filter))
                .SetFields(Fields.Exclude("_id"))
                .SetSortOrder(SortBy.Descending(new string[] { "createddate" }));
        }


        /// <summary>
        /// BuildSWDeploymentQuery
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        private static IMongoQuery BuildSWDistributionQuery(UserFilter filter)
        {
             double utcOffset = DateTimeOffset.Now.Offset.TotalHours;
            var queries = new List<IMongoQuery>();
            if (!String.IsNullOrEmpty(filter.siteid))
            {
                 queries.Add(Query.EQ("sites", filter.siteid));
            }
            if (!String.IsNullOrEmpty(filter.packagename))
            {
                queries.Add(Query.EQ("name", filter.packagename));
            }
            if (!String.IsNullOrEmpty(filter.country))
            {
                queries.Add(Query.EQ("countries", filter.country));
            }

            if (filter.beginTime != DateTime.MinValue)
            {
                queries.Add(Query.GTE("createddate", filter.beginTime));
            }
            if (filter.endTime != DateTime.MinValue)
            {
                queries.Add(Query.LTE("createddate", filter.endTime.AddDays(2)));//.AddHours(utcOffset)
            }

            if (queries.Count == 0)
            {
                return null;
            }
            var queryBuilder = new QueryBuilder<BsonDocument>();
            return queryBuilder.And(queries);
        }


        #endregion


        #region FilteredServiceEvents
        /// <summary>
        /// FilteredServiceEvents
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public List<Message> FilteredServiceEvents(SearchCriteria criteria)
        {
            Philips.HealthCare.IDM.Portal.Logger.LogManager.LogWriter.WriteLog(new LogMessage() { Description = "FilteredServiceEvents - Entry()" });
            List<Message> outMessages = new List<Message>();

            var sort = new BsonDocument 
           {
               {
                   "$sort", new BsonDocument 
                   {
                       {
                           "timestamp", -1
                       }
                   }
               }
           };

            var group = new BsonDocument 
                { 
                    { "$group", 
                        new BsonDocument 
                            { 
                                { 
                                    "_id", new BsonDocument 
									{
										{ "siteid" ,"$siteid" }, 
                                        { "hostname" , "$hostname" },
                                        { "service" ,  "$service" }   
                                    }
                                },
                                {
									"timestamp", new BsonDocument 
									{
										{
											"$last", "$timestamp"
										}
									}
								},
                                {
									"siteid", new BsonDocument 
									{
										{
											"$first", "$siteid"
										}
									}
								},
                                { 
                                    "elementid", new BsonDocument 
                                    {
                                        {
                                            "$first", "$_id"
                                        }
                                    }
                                },                             
                                {
                                    "hostname", new BsonDocument 
                                    {
                                        {
                                            "$first", "$hostname"
                                        }
                                    }
                                },
                                {
                                    "service", new BsonDocument 
                                    {
                                        {
                                            "$first", "$service"
                                        }
                                    }
                                },
                                {
                                    "hostaddress", new BsonDocument 
                                    {
                                        {
                                            "$first", "$hostaddress"
                                        }
                                    }
                                },
                                {
                                    "status", new BsonDocument 
                                    {
                                        {
                                            "$first", "$status"
                                        }
                                    }
                                },
                                {
                                    "type", new BsonDocument 
                                    {
                                        {
                                            "$first", "$type"
                                        }
                                    }
                                },   
                                {
                                    "output", new BsonDocument 
                                    {
                                        {
                                            "$first", "$output"
                                        }
                                    }
                                }
                            }
                    }
                };

            var project = new BsonDocument 
                        { 
                            { 
                                "$project", 
                                new BsonDocument 
                                    { 
                                        {"_id", 0}, 
                                        {"elementid", 1},
                                        {"siteid", 1}, 
                                        {"hostname", 1}, 
                                        {"hostaddress", 1}, 
                                        {"service", 1}, 
                                        {"status", 1}, 
								        {"timestamp", 1},
                                        {"output", 1},
                                        {"type", 1}
                                    } 
                            } 
                        };

            Philips.HealthCare.IDM.Portal.Logger.LogManager.LogWriter.WriteLog(new LogMessage() { Description = "FilteredServiceEvents - Finished framing aggregate query" });
            BsonDocument matches = new BsonDocument();
            matches.AddRange(new BsonDocument("timestamp", new BsonDocument { { "$gte", DateTime.Now.AddDays(Convert.ToInt32(eventsDuration) * -1) } }));

            Philips.HealthCare.IDM.Portal.Logger.LogManager.LogWriter.WriteLog(new LogMessage() { Description = "FilteredServiceEvents - Adding Site Filter to aggregate query" });

            if (string.Compare(criteria.siteid, "All", true) != 0)
            {
                matches.Add(new BsonElement("siteid", criteria.siteid));
            };

            Philips.HealthCare.IDM.Portal.Logger.LogManager.LogWriter.WriteLog(new LogMessage() { Description = "FilteredServiceEvents - Adding Host Service Namespaces Filter to aggregate query" });
            string[] inActiveHosts = GetInactiveHosts().Select(x => x.hostname).ToArray();
            if (inActiveHosts.Length > 0)
            {
                matches.AddRange(new BsonDocument("hostname",
                    new BsonDocument { { "$nin", new BsonArray(inActiveHosts) } }));
            }

            if (!string.IsNullOrEmpty(monitors))
            {
                matches.AddRange(new BsonDocument("service",
                    new BsonDocument { { "$nin", new BsonArray(monitors.Split(',')) } }));
            }

            Philips.HealthCare.IDM.Portal.Logger.LogManager.LogWriter.WriteLog(new LogMessage() { Description = "FilteredServiceEvents - Executing the aggregating query" });
            var match = new BsonDocument { { "$match", matches } };

            var pipeline = new[] { match, sort, group, project };
            var args = new AggregateArgs { AllowDiskUse = false, Pipeline = pipeline };
            var result = eventCollection.Aggregate(args);
            Philips.HealthCare.IDM.Portal.Logger.LogManager.LogWriter.WriteLog(new LogMessage() { Description = "FilteredServiceEvents - Completed Executing the aggregating query" });

            Philips.HealthCare.IDM.Portal.Logger.LogManager.LogWriter.WriteLog(new LogMessage() { Description = "FilteredServiceEvents - Deserializing the aggregating query results" });
            var output = result.Select(BsonSerializer.Deserialize<NotificationMessage>);

            Philips.HealthCare.IDM.Portal.Logger.LogManager.LogWriter.WriteLog(new LogMessage() { Description = "FilteredServiceEvents - Filtering all OK results" });

            if (string.Compare(criteria.status, "All", true) != 0)
            {
                output = (from o in output
                          where o.status == criteria.status
                          select o);
            }
            else
            {
                output = (from o in output
                          where o.status.ToUpper() != "OK"
                          select o);
            }

            Philips.HealthCare.IDM.Portal.Logger.LogManager.LogWriter.WriteLog(new LogMessage() { Description = "FilteredServiceEvents - Adding namespace mapping" });

            if (string.Compare(criteria.service, "All", true) != 0)
            {
                var serviceFilter = nameSpaceCollection.Find(Query.EQ("eventfilter", criteria.service)).
                    SetFields(Fields.Include("servicenamespace")).SetFields(Fields.Exclude("_id")).ToArray();

                var servicenamespaceColl = serviceFilter.Select(x => x.GetElement("servicenamespace")
                    .Value.AsString).ToList<string>();

                output = (from o in output
                          join s in servicenamespaceColl
                              on o.service equals s
                          select o);
            }

            Philips.HealthCare.IDM.Portal.Logger.LogManager.LogWriter.WriteLog(new LogMessage() { Description = "FilteredServiceEvents - Filtering Aggregate check handler and recovery messages" });

            output = (from o in output
                      where (o.type + o.service).ToUpper()
                        != "HANDLERPRODUCT__INTELLISPACE__PACS__ISYNTAXSERVER__AGGREGATE"
                      select o);

            output = (from o in output
                      where (o.type + o.service).ToUpper()
                        != "RECOVERYPRODUCT__INTELLISPACE__PACS__ISYNTAXSERVER__AGGREGATE"
                      select o);

            Philips.HealthCare.IDM.Portal.Logger.LogManager.LogWriter.WriteLog(new LogMessage() { Description = "FilteredServiceEvents - Mapping results with onems data" });

            foreach (var message in output)
            {
                string key = message.siteid + message.hostaddress + message.service + message.status;
                string caseObject =
                    oneEMSAllCasesCollection.ContainsKey(key) ?
                    oneEMSAllCasesCollection[key] : string.Empty;
                outMessages.Add(FormattedMessage(message, caseObject));
            }

            Philips.HealthCare.IDM.Portal.Logger.LogManager.LogWriter.WriteLog(new LogMessage() { Description = "FilteredServiceEvents - Completed Mapping results with onems data" });
            //outMessages.RemoveAll(x => x.servicenamespace ==
            //    "Product__IntelliSpace__PACS__iSyntaxServer__Aggregate"
            //    && x.type == "HANDLER");

            #region Commented
            //            if (output.Count > 0)
            //            {
            //                var client = Utilities.GetConnection();
            //                var mongoDb = client.GetServer().GetDatabase(databaseName);
            //                if (!mongoDb.CollectionExists("tempdata"))
            //                {
            //                    //Making a capped collection of 100MB                    
            //                    mongoDb.CreateCollection("tempdata");
            //                }
            //                var tmpCollection = mongoDb.GetCollection("tempdata");
            //                var options = new MongoInsertOptions()
            //                {
            //                    CheckElementNames = true,
            //                    Flags = InsertFlags.ContinueOnError
            //                };
            //                tmpCollection.RemoveAll(WriteConcern.Acknowledged);
            //                tmpCollection.InsertBatch<NotificationMessage>(output, options);

            //                if (mongoDb.CollectionExists("oneemstickets"))
            //                {
            //                    string map1 = @"function(){ 
            //                            var key = this.siteid + this.hostname + this.hostaddress + this.service;
            //                            var obj = {
            //                                        caseid : this.casenum,
            //                                        elementid : this.elementid,
            //                                        hostaddress : this.hostaddress,
            //                                        hostname : this.hostname,
            //                                        service : this.service,
            //                                        type : this.type,
            //                                        status : this.status,
            //                                        siteid : this.siteid,
            //                                        version : null,
            //                                        sitename : null,
            //                                        output : this.output,
            //                                        state : null,
            //                                        timestamp : this.timestamp,
            //                                        perfdata : null
            //                                      }
            //                            emit(key, obj);
            //                        }";
            //                    string reduce1 = @"function(key, values){ return values; }";

            //                    string map2 = @"function(){ 
            //                            var key = this.siteid + this.hostname + this.hostaddress + this.service;
            //                            var obj = {
            //                                        caseid : this.casenum,
            //                                        elementid : null,
            //                                        hostaddress : this.hostaddress,
            //                                        hostname : this.hostname,
            //                                        service : this.service,
            //                                        type : null,
            //                                        status : null,
            //                                        siteid : this.siteid,
            //                                        version : null,
            //                                        sitename : null,
            //                                        output : null,
            //                                        state : null,
            //                                        timestamp : null,
            //                                        perfdata : null
            //                                      }
            //                            emit(key, obj);
            //                        }";
            //                    string reduce2 = @"function(key, values){ return values; }";


            //                    var mapargs = new MapReduceArgs()
            //                    {
            //                        MapFunction = new BsonJavaScript(map1),
            //                        ReduceFunction = new BsonJavaScript(reduce1),
            //                        OutputMode = MapReduceOutputMode.Replace,
            //                        OutputCollectionName = "final1"
            //                    };

            //                    var mapresult1 = tmpCollection.MapReduce(mapargs);

            //                    mapargs = new MapReduceArgs()
            //                    {
            //                        MapFunction = new BsonJavaScript(map2),
            //                        ReduceFunction = new BsonJavaScript(reduce2),
            //                        OutputMode = MapReduceOutputMode.Merge,
            //                        OutputCollectionName = "final1"
            //                    };

            //                    var mapresult2 = oneEMSTicketsCollection.MapReduce(mapargs);
            //                }

            //            }




            //if (inActiveHosts.Length > 0)
            //{
            //    var filteredServiceEvents = from o in output
            //                                where !(from c in inActiveHosts
            //                                        select c)
            //                                       .Contains(o.hostname)
            //                                select o;

            //    output = filteredServiceEvents;
            //}

            //var serviceEvents = from o in output
            //                    where !(from c in hostMonitors
            //                            select c)
            //                           .Contains(o.service)
            //                    select o;
            //output = serviceEvents;

            //if (string.Compare(criteria.status, "All", true) != 0)
            //{
            //    var data = from o in output
            //               where o.status == criteria.status
            //               select o;
            //    output = data;
            //}
            //else
            //{
            //    var data = from o in output
            //               where o.status.ToUpper() != "OK"
            //               select o;
            //    output = data;
            //}

            //if (string.Compare(criteria.service, "All", true) != 0)
            //{
            //    var data = from o in outMessages
            //               where o.eventfilter == criteria.service
            //               select o;
            //    outMessages = data.ToList<Message>();
            //}

            //if (string.Compare(criteria.status, "All", true) != 0)
            //{
            //    var data = from o in outMessages
            //               where o.status == criteria.status
            //               select o;
            //    outMessages = data.ToList<Message>();
            //}
            //else
            //{
            //    var data = from o in outMessages
            //               where o.status.ToUpper() != "OK"
            //               select o;
            //    outMessages = data.ToList<Message>();
            //}

            //outMessages.RemoveAll(x => x.servicenamespace == 
            //    "Product__IntelliSpace__PACS__iSyntaxServer__Aggregate"
            //    && x.type == "HANDLER");

            //if (string.Compare(criteria.service, "All", true) != 0)
            //{
            //    var data = from o in outMessages
            //               where o.eventfilter == criteria.service
            //               select o;
            //    outMessages = data.ToList<Message>();
            //}
            #endregion Commented

            Philips.HealthCare.IDM.Portal.Logger.LogManager.LogWriter.WriteLog(new LogMessage() { Description = "Sorting results by last received" });
            return outMessages.OrderByDescending(p => p.durationdt).ToList<Message>();
        }
        #endregion

        #region Host Events
        /// <summary>
        /// DefaultHostEvents
        /// </summary>
        /// <returns>List of HostStaus messages</returns>
        public string GetDefaultHostEvents()
        {
            List<Message> outMessages = new List<Message>();
            var output = GetInactiveHosts();

            foreach (var message in output)
            {
                string key = message.siteid + message.hostaddress + message.service + message.status;
                string caseObject =
                    oneEMSAllCasesCollection.ContainsKey(key) ?
                    oneEMSAllCasesCollection[key] : string.Empty;
                outMessages.Add(FormattedMessage(message, caseObject));
            }

            return outMessages.OrderByDescending(p => p.durationdt).ToJson();
        }
        #endregion

        public string GetThrukViewUrl(string siteid)
        {
            return this.GetThrukViewUrl(statesCollection, siteid);
        }
        #endregion

        #region Private Methods

        private IEnumerable<NotificationMessage> GetInactiveHosts()
        {

            IEnumerable<string> values = monitors.Split(',').Select(p => p.Trim());
            BsonDocument matches = new BsonDocument();
            matches.AddRange(new BsonDocument("timestamp", new BsonDocument { { "$gte", DateTime.Now.AddDays(Convert.ToInt32(eventsDuration) * -1) } }));
            var match = new BsonDocument { { "$match", matches } };

            var sort = new BsonDocument 
           {
               {
                   "$sort", new BsonDocument 
                   {
                       {
                           "timestamp", 1
                       }
                   }
               }
           };

            var group = new BsonDocument 
                { 
                    {
                        "$group", 
                        new BsonDocument 
                            { 
                                { 
                                    "_id", new BsonDocument 
									{
										{ "siteid" ,"$siteid" }, 
                                        { "hostname" , "$hostname" }                                        
                                    }
                                },
                                { 
                                    "elementid", new BsonDocument 
									{
										{
											"$last", "$_id"
										}
                                    }
                                },
                                {
									"timestamp", new BsonDocument 
									{
										{
											"$last", "$timestamp"
										}
									}
								},
                                {
									"siteid", new BsonDocument 
									{
										{
											"$last", "$siteid"
										}
									}
								},
								{
									"hostname", new BsonDocument 
									{
										{
											"$last", "$hostname"
										}
									}
								},
								{
									"service", new BsonDocument 
									{
										{
											"$last", "$service"
										}
									}
								},
								{
									"hostaddress", new BsonDocument 
									{
										{
											"$last", "$hostaddress"
										}
									}
								},
								{
									"status", new BsonDocument 
									{
										{
											"$last", "$status"
										}
									}
								},                                
								{
									"output", new BsonDocument 
									{
										{
											"$last", "$output"
										}
									}
								}
                           }
                    }
                };

            var project = new BsonDocument 
                        { 
                            { 
                                "$project", 
                                new BsonDocument 
                                    { 
                                        {"_id", 0}, 
                                        {"elementid", 1},
                                        {"siteid", 1}, 
								        {"hostname", 1}, 
								        {"hostaddress", 1}, 
								        {"service", 1}, 
								        {"status", 1}, 
								        {"timestamp", 1},
                                        {"output", 1}
                                    } 
                            } 
                        };
            var pipeline = new[] { match, sort, group, project };
            var args = new AggregateArgs { Pipeline = pipeline, AllowDiskUse = true };
            var result = eventCollection.Aggregate(args);
            var output = result.Select(BsonSerializer.Deserialize<NotificationMessage>);

            var hostMonitors = monitors.Split(',').Select(p => p.Trim());
            var serviceEvents = from o in output
                                where (from c in hostMonitors
                                       select c)
                                       .Contains(o.service)
                                select o;

            var data = from o in serviceEvents
                       where (o.status.ToUpper() == "CRITICAL" || o.status.ToUpper() == "DOWN")
                       select o;
            output = data;

            return output;
        }

        private static MongoClient GetConnection()
        {
            try
            {
                var connectionString = ConfigurationManager.AppSettings["MongoDbConnectionString"];
                return new MongoClient(connectionString);
            }
            catch (ConfigurationErrorsException ex)
            {
                Logger.Error(string.Format("Error while trying to read the MongoDBConnectionString from the config file", ex));
                return null;
            }
        }

        private static MongoDatabase GetSpecificDatabase(string databaseName)
        {
            MongoClient mongoClient;
            MongoDatabase mongoDB = null;
            try
            {
                mongoClient = GetConnection();
                if (mongoClient != null)
                {
                    mongoDB = mongoClient.GetServer().GetDatabase(databaseName);
                }
                return mongoDB;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("Error while trying to read the get the connection for the Mongo DB - " + databaseName, ex));
                return null;
            }
        }

        private static MongoCollection<BsonDocument> GetSpecificCollection(string databaseName, string collectionName)
        {
            MongoDatabase mongoDB = null;
            MongoCollection<BsonDocument> mongoCollection = null;
            try
            {
                mongoDB = GetSpecificDatabase(databaseName);
                if (mongoDB != null)
                {
                    mongoCollection = mongoDB.GetCollection<BsonDocument>(collectionName);
                }
                return mongoCollection;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("Error while trying to read the get the collection - " + collectionName + " from the Mongo DB - " + databaseName, ex));
                return null;
            }
        }

        private static IMongoQuery BuildQuery(UserFilter filter)
        {
            double utcOffset = DateTimeOffset.Now.Offset.TotalHours;
            var queries = new List<IMongoQuery>();
            if (!String.IsNullOrEmpty(filter.siteid))
            {
                queries.Add(Query.EQ("siteid", filter.siteid));
            }
            if (!String.IsNullOrEmpty(filter.hostname))
            {
                queries.Add(Query.EQ("hostname", filter.hostname));
            }
            if (filter.service != null && filter.service != string.Empty)
            {
                queries.Add(Query.EQ("service", filter.service));
            }
            if (filter.serviceList != null)
            {
                queries.Add(Query.In("service", new BsonArray(filter.serviceList)));
            }
            if (!String.IsNullOrEmpty(filter.status))
            {
                queries.Add(Query.EQ("status", filter.status));
            }
            if (!String.IsNullOrEmpty(filter.type))
            {
                queries.Add(Query.EQ("type", filter.type));
            }
            if (filter.beginTime != DateTime.MinValue)
            {
                queries.Add(Query.GTE("timestamp", filter.beginTime.AddHours(utcOffset)));
            }
            if (filter.endTime != DateTime.MinValue)
            {
                queries.Add(Query.LTE("timestamp", filter.endTime.AddHours(utcOffset)));
            }
            if (!String.IsNullOrEmpty(filter.hostaddress))
            {
                queries.Add(Query.EQ("hostaddress", filter.hostaddress));
            }
            if (queries.Count == 0)
            {
                return null;
            }
            var queryBuilder = new QueryBuilder<BsonDocument>();
            return queryBuilder.And(queries);
        }

        private IEnumerable<BsonValue> GetAllSites(MongoCollection<BsonDocument> mongoCollection)
        {
            return mongoCollection.Distinct("siteid");
        }



        private IEnumerable<BsonValue> GetAllStatus(MongoCollection<BsonDocument> mongoCollection)
        {
            return mongoCollection.Distinct("status");
        }

        private IEnumerable<BsonValue> GetAllServices(MongoCollection<BsonDocument> mongoCollection, string serviceFilter)
        {
            return mongoCollection.Distinct("servicenamespace", Query.EQ("eventfilter", serviceFilter));
        }

        private string GetThrukViewUrl(MongoCollection<BsonDocument> mongoCollection, string siteKey)
        {
            var queries = new List<IMongoQuery>();

            if (String.IsNullOrEmpty(siteKey))
            {
                return String.Empty;
            }
            queries.Add(Query.EQ("siteid", siteKey));
            queries.Add(Query.EQ("hostname", "localhost"));

            var queryBuilder = new QueryBuilder<BsonDocument>();
            try
            {
                if (mongoCollection != null)
                {
                    //TO DO: hostname should be changed to ipaddress - once it is available. 
                    var ipAddress = mongoCollection.Find(queryBuilder.And(queries)).SetLimit(1);
                    if (ipAddress.Count() != 0)
                    {
                        foreach (var ip in ipAddress)
                        {
                            return thrukViewUrl.Replace("[ipaddress]", ip[nebNodeNamespace].ToString());
                        }
                    }
                    else
                    {
                        throw new Exception("Site View is not available");
                    }
                }
            }
            catch (Exception ex)
            {
                //TO DO: log exception 
                throw ex;
            }
            return String.Empty;
        }

        private BsonDocument GetServiceDisplayName(MongoCollection<BsonDocument> mongoCollection, string serviceName)
        {
            return mongoCollection.FindOne(Query.EQ("servicenamespace", serviceName));
        }

        private BsonDocument GetSiteInformation(MongoCollection<BsonDocument> mongoCollection, string siteid)
        {
            return mongoCollection.FindOne(Query.EQ("siteid", siteid));
        }

        private IEnumerable<BsonValue> GetAllNodes(MongoCollection<BsonDocument> mongoCollection, string siteId)
        {
            if (siteId != null)
            {
                return mongoCollection.Distinct("hostname", Query.EQ("siteid", siteId));
            }
            else
            {
                return mongoCollection.Distinct("hostname");
            }
        }

        private IEnumerable<BsonValue> GetCountryList(MongoCollection<BsonDocument> mongoCollection)
        {
            return mongoCollection.FindAll();
        }

        private Dictionary<string, string> GetAllCasesCollection(MongoCollection<BsonDocument> mongoCollection)
        {
            Dictionary<string, string> caseCollection = new Dictionary<string, string>();
            foreach (var item in mongoCollection.Find(Query.EQ("oneemsstatus", "Open")))
            {
                try
                {
                    string key = item.GetElement("siteid").Value.AsString
                        + item.GetElement("hostaddress").Value.AsString
                        + item.GetElement("service").Value.AsString
                        + item.GetElement("status").Value.AsString;
                    caseCollection.Add(key,
                        item.GetElement("case_number").Value.AsString
                        + "," + item.GetElement("tickettype").Value.AsString
                        + "," + item.GetElement("oneemsstatus").Value.AsString);
                }
                catch (Exception)
                {
                    continue;
                }
            }
            return caseCollection;
        }

        private MongoCursor<BsonDocument> GetAllMessages(MongoCollection<BsonDocument> mongoCollection, string siteid)
        {
            UserFilter filter = new UserFilter();
            filter.siteid = siteid;
            return mongoCollection.Find(BuildQuery(filter)).SetFields(Fields.Exclude("_id", "timestamp")).SetSortOrder(SortBy.Descending(new string[] { "timestamp" }));
        }

        private IEnumerable<BsonValue> GetServiceFilters(MongoCollection<BsonDocument> mongoCollection)
        {
            return mongoCollection.Distinct("eventfilter");
        }

        private MongoCursor<BsonDocument> GetAllMessagesForNode(MongoCollection<BsonDocument> mongoCollection, string hostName)
        {
            UserFilter filter = new UserFilter();
            filter.hostname = hostName;
            return mongoCollection.Find(BuildQuery(filter)).SetFields(Fields.Exclude("_id", "timestamp")).SetSortOrder(SortBy.Descending(new string[] { "timestamp" }));
        }

        private Message FormattedMessage(NotificationMessage message, string caseObject)
        {

            //BsonDocument document = GetServiceDisplayName(nameSpaceCollection, message.service);
            //string namespaceAbbreviation = document != null ?
            //    document.GetElement("displayname").Value.ToString() : message.service;`


            string namespaceAbbreviation = "NA";
            string eventFilter = "NA";
            string siteName = "NA";
            string siteVersion = "NA";

            //string eventFilter = document != null ?
            //    document.GetElement("eventfilter").Value.ToString() : message.service;

            //document = GetSiteInformation(siteCollection, message.siteid);
            //string siteName = document != null ?
            //    document.GetElement("sitename").Value.ToString() : "Not Available";

            //string siteVersion = document != null ?
            //    document.GetElement("siteversion").Value.ToString() : "NA";

            return new Message()
            {
                elementid = message.elementid != null ? message.elementid.ToString() : null,
                caseid = !string.IsNullOrEmpty(caseObject) ? caseObject.Split(',')[0] : "?",
                casetype = !string.IsNullOrEmpty(caseObject) ? caseObject.Split(',')[1] : "",
                sitekey = message.siteid,
                sitename = siteName,
                version = siteVersion.Replace(',', '.'),
                //version = siteVersion,
                hostipaddress = message.hostaddress,
                hostname = message.hostname.IndexOf('.') > 0 ?
                    message.hostname.Substring(0, message.hostname.IndexOf('.')) : message.hostname,
                //description = message.output == null ? string.Empty :
                //    message.output.Substring(0, (message.output.Length < 50 ? message.output.Length : 50)),
                description = message.output == null ? string.Empty : message.output,
                servicenamespace = message.service,
                eventfilter = eventFilter,
                namespaceabbreviation = namespaceAbbreviation,
                lastreceived = message.timestamp.ToString("dd MMM yyyy hh:mm:ss tt"),
                duration = DateTimeHelper.CalculateDurationString(message.timestamp),
                durationdt = message.timestamp.ToString("yyyy-MM-dd HH:mm:ss"),
                status = message.status,
                type = message.type
            };
        }

        private MongoCursor<NotificationMessage> GetEventsByDateRange(MongoCollection<BsonDocument> mongoCollection, UserFilter filter)
        {
            return mongoCollection.FindAs<NotificationMessage>(BuildQuery(filter)).SetFields(Fields.Exclude("_id"));
        }
        #endregion
    }
}

