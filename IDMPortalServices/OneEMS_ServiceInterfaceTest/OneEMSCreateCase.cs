using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Philips.HealthCare.IDM.Portal.OneEMS_ServiceInterfaceTest
{
    /// <summary>
    /// Class to create Cases in OneEMS EII specific WSDL
    /// </summary>
    public class OneEMSCreateCase
    {
        private EIICreateCase.EIICaseCreationServiceService myCase = new EIICreateCase.EIICaseCreationServiceService();

        public string createCase(string cctechnicalID, 
            string ccCaseTitle, 
            string ccCaseNotes, 
            string ccPriority, 
            string ccSessionID,
            string ccSessionURL)
        {
            try
            {
                if (string.IsNullOrEmpty(ccSessionID))
                {
                    return "Invalid SesssionID";
                }
                
                #region[Consume Custome webservice]
                myCase.SessionHeaderValue = new EIICreateCase.SessionHeader();
                myCase.SessionHeaderValue.sessionId = ccSessionID;
                //Get server alias from full URL, since server alias name is not directly available
                string sessionServer = ccSessionURL.Substring(ccSessionURL.IndexOf("//") + 2);
                sessionServer = sessionServer.Remove(sessionServer.IndexOf('/'));

                myCase.Url = ConfigurationManager.AppSettings["EIICreateCaseURL"]
                    .Replace("$$ServerAlias$$", sessionServer);

                EIICreateCase.ReqParamForCase rpfc = new EIICreateCase.ReqParamForCase();

                rpfc.CaseTitle = ccCaseTitle;
                rpfc.TechnicalId = cctechnicalID;
                rpfc.CaseNotes = ccCaseNotes;
                rpfc.Priority = ccPriority;

                EIICreateCase.ResponseInfoForCreateCase newTicket = myCase.CreateCase(rpfc);

                #endregion
                return newTicket.CaseNumber;
            }
            catch (Exception e1)
            {
                System.Console.WriteLine("Ticket Creation Exception: " + e1.Message);
                return "0";
            }
        }
    }
}