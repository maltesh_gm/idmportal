﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Philips.HealthCare.IDM.Portal.OneEMS_ServiceInterface;

namespace Philips.HealthCare.IDM.Portal.OneEMS_ServiceInterface
{
    /// <summary>
    /// Class to connect to OneEMS and get sessionID
    /// </summary>
    public class OneEMSLogin
    {
        private OneEMSEnterprise.SforceService binding;
        private OneEMSEnterprise.LoginResult lr;

        public void login(string userName, string UserPassword)
        {
            try
            {
                binding = new OneEMSEnterprise.SforceService();
                lr = binding.login(userName, UserPassword);
                //lr = binding.login("ei_proactive@philips.com.emsmq3", "philip$2013WwfSmaQvEEjBJ63XvhsJalTZe");
            }
            catch (Exception e)
            {
                System.Console.WriteLine("Login Exception: " + e.Message);
                new Exception("Login to SalesForce failed.");
            }
        }

        public string getSessionID { get { return lr.sessionId; } }
        public string getSessionURL { get { return lr.serverUrl; } }

        public void logout()
        {
            try
            {
                binding.logout();
            }
            catch (Exception e)
            {
                System.Console.WriteLine("Logout Exception: " + e.Message);
            }
        }
    }
}
