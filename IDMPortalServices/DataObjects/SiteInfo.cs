﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.DataObjects {
    /// <summary>
    /// SiteInfo
    /// </summary>
    public class SiteInfo {
        /// <summary>
        /// siteid
        /// </summary>
        public string siteid { get; set; }
        /// <summary>
        /// sitename 
        /// </summary>
        public string sitename { get; set; }
        /// <summary>
        /// siteversion 
        /// </summary>
        public string siteversion { get; set; }
        /// <summary>
        /// Country
        /// </summary>
        public Country country;
        /// <summary>
        /// sitestatus 
        /// </summary>
        public string sitestatus { get; set; }
    }

    public class ThrukView
    {
        public string Url { get; set; }
    }

   
}
