﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.DataObjects
{
    public class Node
    {
        public string type { get; set; }
        public string count { get; set; }
        public List<string> ipaddress { get; set; }
    }

    public class Infrastructure
    {
        public List<Node> nodes { get; set; }

        public Infrastructure()
        {
            nodes = new List<Node>();
        }
    }

    public class Feature
    {
        public string name { get; set; }
        public string version { get; set; }
        public string installpath { get; set; }
    }

    public class Featurelist
    {
        public List<Feature> features { get; set; }

        public Featurelist()
        {
            features = new List<Feature>();
        }
    }

    public class SWDRequest
    {
        public string customer { get; set; }
        public string siteid { get; set; }
        public Infrastructure infrastructure { get; set; }
        public Featurelist featurelist { get; set; }
    }

    /********* New Version of Data Structure ***********/

    public class Key
    {
        public string encrypted { get; set; }
        public string keyname { get; set; }
        public string text { get; set; }
    }

    public class SiteBagKeys
    {
        public List<Key> key { get; set; }
    }

    public class SiteBag
    {
        public SiteBagKeys keys { get; set; }
    }

    public class PackageInfo
    {
        public string name { get; set; }
        public string version { get; set; }
    }

    public class Packages
    {
        public List<PackageInfo> packageinfo { get; set; }
    }

    public class RoleBagKeys
    {
        public List<Key> key { get; set; }
    }

    public class RoleBag
    {
        public RoleBagKeys keys { get; set; }
    }

    public class Role
    {
        public string roletype { get; set; }
        public string ipv4 { get; set; }
        public string fqdn { get; set; }
        public Packages packages { get; set; }
        public RoleBag rolebag { get; set; }
    }

    public class Roles
    {
        public List<Role> role { get; set; }
    }

    public class SiteMap
    {
        public string siteid { get; set; }
        public string sitename { get; set; }
        public string status { get; set; }
        public SiteBag sitebag { get; set; }
        public Roles roles { get; set; }
    }

    public class SiteMapObject
    {
        public SiteMap sitemap { get; set; }
    }

    public class SiteMapObjectID
    {
        public object _id { get; set; }
        public SiteMap sitemap { get; set; }
    }
}
