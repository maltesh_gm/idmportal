﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.DataObjects
{
  
    /// <summary>
    /// Contract for Software Deployment
    /// </summary>
    public class SWDeployment
    {
        /// <summary>
        /// Constructor for Software deployment
        /// </summary>
        public SWDeployment()
        {
            hosts = new List<string>();
        }

        /// <summary>
        /// siteid
        /// </summary>
        public string siteid { get; set; }

        /// <summary>
        /// pkg name
        /// </summary>
        public string packagename { get; set; }

        /// <summary>
        /// version
        /// </summary>
        public string version { get; set; }

        /// <summary>
        /// hosts
        /// </summary>
        public List<string> hosts { get; set; }

        /// <summary>
        /// pkg status
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// createdby
        /// </summary>
        public string createdby { get; set; }

        /// <summary>
        ///createddate
        /// </summary>
        //[BsonDateTimeOptions(Representation = BsonType.String, Kind = DateTimeKind.Utc)]
        public DateTime createddate { get; set; }

        /// <summary>
        /// createddatestring
        /// </summary>        
        public string createddatestring
        {
            get
            {
                return createddate.ToString("dd MMM yyyy HH:mm:ss tt");
            }
        }

        /// <summary>
        /// modifiedby
        /// </summary>
        public string modifiedby { get; set; }

        ///<summary>
        ///lastmodified
        ///</summary>
        // [BsonDateTimeOptions(Representation = BsonType.String, Kind = DateTimeKind.Utc)]
        public DateTime lastmodified { get; set; }

        /// <summary>
        /// perfdata
        /// </summary>     
        public List<Perfdata> perfdata { get; set; }        
    }    
}
