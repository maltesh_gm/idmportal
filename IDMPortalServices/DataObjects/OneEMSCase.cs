﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.DataObjects
{
    public class OneEMSCase
    {
        public string events_id { get; set; }
        
        public string siteid { get; set; }

        public string hostname { get; set; }

        public string hostaddress { get; set; }

        public string service { get; set; }

        public string output { get; set; }

        public string case_number { get; set; }

        public string case_title { get; set; }

        public string priority { get; set; }

        public string status { get; set; }

        public string oneemsstatus { get; set; }

        public string tickettype { get; set; }

        public DateTime casedate { get; set; }

        public string createdby { get; set; }
    }

    public class OneEMSTicket
    {
        public string caseid { get; set; }
        public string casenumber { get; set; }
        public string accountid { get; set; }
        public string assetid { get; set; }
        public string productid { get; set; }
        public string contactid { get; set; }
        public string ownerid { get; set; }
        public string type { get; set; }
        public string priority { get; set; }
        public string origin { get; set; }
        public string subject { get; set; }
        public string description { get; set; }
        public string status { get; set; }
        public string reason { get; set; }
        public string service_type__c { get; set; }
        public bool? known_error_id__c { get; set; }
        public string impact__c { get; set; }
        public string pending_status__c { get; set; }
        public DateTime? closeddate { get; set; }
        public DateTime? createddate { get; set; }
        public DateTime? lastmodifieddate { get; set; }
    }
}
