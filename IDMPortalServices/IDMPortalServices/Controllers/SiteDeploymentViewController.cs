﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Logger;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class SWDDeploymentController : ApiController
    {
        ResponseObject responseObject;

            /// <summary>
        /// Initializes a new instance of the <see cref="SitesController"/> class.
        /// </summary>
        public SWDDeploymentController()
        {  
            responseObject = new ResponseObject();
        }

        public HttpResponseMessage Post([FromBody] string siteKey) {
            try
            {
                
                responseObject.status = 1;
                responseObject.message = "success";
                responseObject.payload = "{'siteid' : 'AHR00', 'nodes' : [ { 'nodename' : 'ahr00-am001',  'nodetype' : 'ArchiveMirror', 'nodeip' : '192.168.225.37','nodelocation' : 'MainLocation', 'version' : 'NA', 'applications' :  [ { 'applicationname': 'anywhere', 'version': '1.2' }, { 'applicationname': 'volumevision', 'version': '2.0' } ]}, { 'nodename' : 'ahr00-am002',  'nodetype' : 'ArchiveMirror', 'nodeip' : '192.168.225.38', 'nodelocation' : 'MainLocation', 'version' : 'NA', 'applications' :  [  { 'applicationname': 'anywhere', 'version': '1.2'} ] }], 'packages' : [ { 'packagename': 'All', 'version': '0.0' },{ 'packagename': 'anywhere', 'version': '1.3' }, { 'packagename': 'volumevision', 'version': '2.0',}, { 'packagename': 'McAfee', 'version': '3.0' } ]}"; ;
            }
            catch (Exception ex)
            {
                responseObject.status = 0;
                responseObject.message = ex.Message;
                responseObject.payload = string.Empty;
            }

            return new HttpResponseMessage()
            {
                Content = new JsonContent(
                    JObject.Parse(responseObject.payload)
                    )
            };
        }        
    }
}