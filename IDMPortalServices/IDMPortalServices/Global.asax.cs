﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;

namespace Philips.HealthCare.IDM.Portal.Services {
  public class Global : System.Web.HttpApplication {

    protected void Application_Start(object sender, EventArgs e) {
      WebApiConfig.Register(GlobalConfiguration.Configuration);
      StaticCache.LoadStaticCache();
    }

    protected void Session_Start(object sender, EventArgs e) {
        //StaticCache.LoadStaticCache();
    }

    protected void Application_BeginRequest(object sender, EventArgs e) {
        //StaticCache.LoadStaticCache();
    }

    protected void Application_AuthenticateRequest(object sender, EventArgs e) {

    }

    protected void Application_Error(object sender, EventArgs e) {

    }

    protected void Session_End(object sender, EventArgs e) {

    }

    protected void Application_End(object sender, EventArgs e) {

    }
  }
}