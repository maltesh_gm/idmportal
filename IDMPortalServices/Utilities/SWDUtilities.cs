﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Philips.HealthCare.IDM.Portal.DataObjects;

namespace Philips.HealthCare.IDM.Portal.Utilities
{
    public static class SWDUtilities
    {
        public static string GetXMLForSiteMap(SiteMapObject siteMap)
        {
            XDocument doc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("SiteMap",
                    new XElement("SiteBag",
                        new XElement("Keys",
                            from siteBagKey in siteMap.sitemap.sitebag.keys.key
                            select new XElement("Key", siteBagKey.text,
                                new XAttribute("Encrypted", siteBagKey.encrypted),
                                new XAttribute("KeyName", siteBagKey.keyname)))),
                    new XElement("Roles",
                        from roleBagRoles in siteMap.sitemap.roles.role
                        select new XElement("Role",
                            new XAttribute("RoleType", roleBagRoles.roletype),
                            new XElement("IPv4", roleBagRoles.ipv4),
                            new XElement("FQDN", roleBagRoles.fqdn),
                            new XElement("Packages",
                                from roleBagPkgs in roleBagRoles.packages.packageinfo
                                select new XElement("PackageInfo",
                                    new XAttribute("Name", roleBagPkgs.name),
                                    new XAttribute("Version", roleBagPkgs.version))),
                            new XElement("RoleBag",
                                new XElement("Keys",
                                    from roleBagKey in roleBagRoles.rolebag.keys.key
                                    select new XElement("Key", roleBagKey.text,
                                        new XAttribute("Encrypted", roleBagKey.encrypted),
                                        new XAttribute("KeyName", roleBagKey.keyname))))))));
            return doc.ToString();
        }
    }
}
