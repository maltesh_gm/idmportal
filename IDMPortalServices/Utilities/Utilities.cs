﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack;
using System.Configuration;
using System.Collections.Specialized;
using Philips.HealthCare.IDM.Portal.DataObjects.Model;

namespace Philips.HealthCare.IDM.Portal.Utilities
{
    public class UtilityClass
    {
        public static object DeserializeJsonMessage(
            string msgType,
            string json)
        {
            object messageObj;
            try
            {
                switch (msgType)
                {
                    case "ServiceMsg":
                        messageObj =
                            ServiceStack.Text.JsonSerializer.DeserializeFromString<EventNotificationHolder>(json);
                        break;
                    case "GeneralMsg":
                        messageObj =
                            ServiceStack.Text.JsonSerializer.DeserializeFromString<NotificationMessage>(json);
                        break;
                    case "MailMessage":
                        messageObj =
                            ServiceStack.Text.JsonSerializer.DeserializeFromString<NagiosMailMessage>(json);
                        break;
                    default:
                        messageObj = null;
                        break;
                }

                return messageObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string SerializeToJsonMessage(
            string msgType,
            object serializeObj)
        {
            string jsonMessage;
            try
            {
                switch (msgType)
                {
                    case "MailMessage":
                        jsonMessage = 
                            ServiceStack.Text.JsonSerializer.SerializeToString<NagiosMailMessage>((NagiosMailMessage)serializeObj);
                        break;
                    default:
                        jsonMessage = string.Empty;
                        break;
                }
                return jsonMessage;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}